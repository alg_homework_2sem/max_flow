/*
 * Задан ориентированный граф, каждое ребро которого обладает целочисленной пропускной способностью.
 * Найдите максимальный поток из вершины с номером 1 в вершину с номером 𝑛.
 * Алгоритм Диница. Сложность - О(V^2 * E). Память - O(V + E) 
*/
#include <iostream>
#include <queue>
#include <vector>

using std::queue;
using std::vector;

class DinicMaxFlow {
 private:
  struct edge { // Храним поток прямо тут 
    int from, to;
    long long capaticity, flow;
    edge(int fr, int to, int cap, int fl)
        : from(fr), to(to), capaticity(cap), flow(fl) {}
  };
  vector<vector<int> > graph;
  vector<edge> edges;
  int source, target;
  long long INF;
  int verticesCount;
  vector<int> distances;
  vector<int> lastDeleted;
  long long dfs(int vertex, long long pushFlow);
  int bfs();

 public:
  DinicMaxFlow(int verticesCount, int maxCap, int source, int target);
  void addEdge(int start, int finish, long long cap);
  long long findMaxFlow();
};

int main() {
  int verticesCount, edgeCount;
  std::cin >> verticesCount >> edgeCount;
  DinicMaxFlow maxFlow(verticesCount, 1e8, 0, verticesCount - 1);
  for (int i = 0; i < edgeCount; i++) {
    int from, to, cap;
    std::cin >> from >> to >> cap;
    maxFlow.addEdge(from - 1, to - 1, cap);
  }
  std::cout << maxFlow.findMaxFlow() << std::endl;
  return 0;
}

DinicMaxFlow::DinicMaxFlow(int verticesCount, int maxCap, int source,
                           int target)
    : graph(verticesCount),
      source(source),
      target(target),
      INF(maxCap + 1),
      verticesCount(verticesCount),
      distances(verticesCount),
      lastDeleted(verticesCount) {}

void DinicMaxFlow::addEdge(int start, int finish, long long cap) {
  edges.emplace_back(start, finish, cap, 0);
  graph[start].push_back(edges.size() - 1);
  edges.emplace_back(finish, start, 0, 0);
  graph[finish].push_back(edges.size() - 1);
}

long long DinicMaxFlow::dfs(int vertex, long long pushFlow) {
  if (vertex == target || pushFlow == 0) return pushFlow;
  for (; lastDeleted[vertex] < graph[vertex].size();
       lastDeleted[vertex]++) {  // Будем выбрасывать ребра вдоль которых не получится добратся до стока
    edge cur_edge = edges[graph[vertex][lastDeleted[vertex]]];
    int neigh = cur_edge.to;
    if (distances[neigh] == distances[vertex] + 1) {
      long long pushed = dfs(neigh, std::min(1LL * pushFlow, cur_edge.capaticity - cur_edge.flow));
      if (pushed) {
        edges[graph[vertex][lastDeleted[vertex]]].flow += pushed;
        edges[graph[vertex][lastDeleted[vertex]] ^ 1].flow += -pushed;  // Обратное ребро
        return pushed;
      }
    }
  }
  return 0;
}

int DinicMaxFlow::bfs() {
  distances.assign(verticesCount, -1);
  queue<int> curQueue;
  curQueue.push(source);
  distances[source] = 0;
  while (!curQueue.empty()) {
    int curVertex = curQueue.front();
    curQueue.pop();
    for (int i = 0; i < graph[curVertex].size(); i++) {
      edge cur_edge = edges[graph[curVertex][i]];
      int neigh = cur_edge.to;
      if (distances[neigh] == -1 && cur_edge.flow < cur_edge.capaticity) {
        curQueue.push(neigh);
        distances[neigh] = distances[curVertex] + 1;
      }
    }
  }
  return distances[target];
}

long long DinicMaxFlow::findMaxFlow() {
  long long maxflow = 0;
  while (1) {
    lastDeleted.assign(verticesCount, 0);
    if (bfs() == -1) break;  // Не найден путь в слоистой сети
    while (int pushed = dfs(source, INF))
      maxflow += pushed;
  }
  return maxflow;
}

